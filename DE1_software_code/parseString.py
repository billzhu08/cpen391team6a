#read the raw flight data from data.txt and store it to mystr
a = open("data.txt", "r")
mystr = a.read()

#locate and store all the useful flight date into a list
strings = list()
while len(mystr) > 1:
    f = mystr.find('flight')
    strings.append(mystr[f+9:f+15])
    t = mystr.find('time2')
    strings.append(mystr[t+8:t+13])
    la = mystr.find('latitude')
    lo = mystr.find('longtitude')
    al = mystr.find('altitude')
    strings.append(mystr[la+11:lo-3])  
    strings.append(mystr[lo+13:al-3])
    end = mystr.find('}')
    mystr = mystr[end+2:]

#create allFlights.txt file to store the processed flught data
f = open("allFlights.txt", "w")
for x in strings:
    f.write(x+'\n')
    
f.close() 
