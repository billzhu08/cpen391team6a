/*************************************************************************************************
** This function draws a single ASCII character at the coord and colour specified
** it optionally ERASES the background colour pixels to the background colour
** This means you can use this to erase characters
**
** e.g. writing a space character with Erase set to true will set all pixels in the
** character to the background colour
**
*************************************************************************************************/
#include <stdio.h>
#include "Graphics.h"

// Constants ideally put this in a header file and #include it
#define XRES 		800
#define YRES 		480

#define WIDTH  	22
#define HEIGHT 	40
#define NUM_PER_COL 3

// declaration of the external Array, ideally put this in a header file and #include it

extern const unsigned char Font22x40[];


void OutGraphicsCharFont4(int x, int y, int fontcolour, int backgroundcolour, int c, int Erase)
{
// using register variables (as opposed to stack based ones) may make execution faster
// depends on compiler and CPU

	register int row, column, theX = x, theY = y ;
	register unsigned int pixels, theC = c;
	register char theColour = fontcolour  ;
	register unsigned int BitMask;
	register int index;

// if x,y coord off edge of screen don't bother

    if(((short)(x) > (short)(XRES-1)) || ((short)(y) > (short)(YRES-1)))
        return ;


// if printable character subtract hex 20
	if(((short)(theC) >= (short)(' ')) && ((short)(theC) <= (short)('~'))) {
		theC = theC - 0x20 ;
		index = theC * HEIGHT * NUM_PER_COL;
		for(row = 0; (row) < (HEIGHT); row ++)	{
			BitMask = 128 ; // MSB of the a byte

			for(column = 0; (column) < (WIDTH); column ++)	{

				// get the bit pattern for row 0 of the character from the software font
				pixels = Font22x40[index + row*NUM_PER_COL + (column / 8)];
				// if a pixel in the character display it
				if((pixels & BitMask) != (char)0)
					WriteAPixel(theX+column, theY+row, theColour) ;

				else {
					if(Erase == 1)

						// if pixel is part of background (not part of character)
						// erase the background to value of variable BackGroundColour

						WriteAPixel(theX+column, theY+row, backgroundcolour) ;
				}
				BitMask = BitMask >> 1 ;
				if (BitMask == 0) {
					BitMask = 128; // done with the current byte
				}
			}
		}
	}
}

