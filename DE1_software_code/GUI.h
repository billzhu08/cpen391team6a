#ifndef GUI_H_
#define GUI_H_

#include "flightTracker.h"

// headings
#define NORTH 1
#define EAST  2
#define SOUTH 3
#define WEST  4

// The range of XY coordinates that are displayed on the screen
#define MIN_LATITUDE	 49000
#define MAX_LATITUDE	 49250
#define MIN_LONGITUDE	 123000
#define MAX_LONGITUDE	 123100

// Length of each field(excluding the '\0')
#define LongLen 10
#define LatLen 8
#define DateLen 10
#define TimeLen 5
#define FIDLen 6

// function to display menu page
void displayMenuPage();
// function to display list page
void displayListPage(Flight flights[], int numFlight);
// function to display list page
void displayFollowPage(Flight flights[], int numFlight);
// function to display search

// function to display map page
void displayMapPage(Flight flights[], int numFlights);
//function to display credits displayMapPage
void displayCreditsPage(void);



// function to display plane
void drawPlanes(Flight flights[], int numFlights);
// function to display button
void drawButton(char* text, int x, int y, int backgroundColour, int fontColour);

//function to display our program's UI header
void drawHeader();

// functions to display planes facing different directions
void drawPlaneNorth(Flight flight);
void drawPlaneSouth(Flight flight);
void drawPlaneEast(Flight flight);
void drawPlaneWest(Flight flight);
#endif
