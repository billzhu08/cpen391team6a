#ifndef GRAPHICS_H_
#define GRAPHICS_H_

#include <stdio.h>
#include <stdlib.h>
#include "flightTracker.h"

// graphics register addresses

#define GraphicsCommandReg   		(*(volatile unsigned short int *)((virtual_base + ((0x10000 ) &  (HW_REGS_MASK )))))
#define GraphicsStatusReg   		(*(volatile unsigned short int *)((virtual_base + ((0x10000 ) &  (HW_REGS_MASK )))))
#define GraphicsX1Reg   		(*(volatile unsigned short int *)((virtual_base + ((0x10002 ) &  (HW_REGS_MASK )))))
#define GraphicsY1Reg   		(*(volatile unsigned short int *)((virtual_base + ((0x10004 ) &  (HW_REGS_MASK )))))
#define GraphicsX2Reg   		(*(volatile unsigned short int *)((virtual_base + ((0x10006 ) &  (HW_REGS_MASK )))))
#define GraphicsY2Reg   		(*(volatile unsigned short int *)((virtual_base + ((0x10008 ) &  (HW_REGS_MASK )))))
#define GraphicsColourReg  		(*(volatile unsigned short int *)((virtual_base + ((0x1000E ) &  (HW_REGS_MASK )))))
#define GraphicsBackGroundColourReg 		(*(volatile unsigned short int *)((virtual_base + ((0x100010 ) &  (HW_REGS_MASK )))))

/************************************************************************************************
** This macro pauses until the graphics chip status register indicates that it is idle
***********************************************************************************************/

#define WAIT_FOR_GRAPHICS		while((GraphicsStatusReg & 0x0001) != 0x0001);

// #defined constants representing values we write to the graphics 'command' register to get
// it to draw something. You will add more values as you add hardware to the graphics chip
// Note DrawHLine, DrawVLine and DrawLine at the moment do nothing - you will modify these

#define DrawHLine		1
#define DrawVLine		2
#define DrawLine		3
#define DrawRectangle	4
#define	PutAPixel		0xA
#define	GetAPixel		0xB
#define	ProgramPaletteColour    0x10

// defined constants representing colours pre-programmed into colour palette
// there are 256 colours but only 8 are shown below, we write these to the colour registers
//
// the header files "Colours.h" contains constants for all 256 colours
// while the course file "ColourPaletteData.c" contains the 24 bit RGB data
// that is pre-programmed into the palette

#define	BLACK			0
#define	WHITE			1
#define	RED				2
#define	LIME			3
#define	BLUE			4
#define	YELLOW		5
#define	CYAN			6
#define	MAGENTA		7
#define GREY		  8

// screen dimentions
#define display_cols	799
#define display_rows	479


void clearScreen(int colour);
void WriteAPixel(int x, int y, int Colour);
void drawRectangle(int x, int y, int width, int height, int colour);
void drawRectangleBoarder(int x, int y, int width, int height, int colour, int boarder);
void drawTriangle(int x, int y, int height, int colour);
void drawLine(int x1, int x2, int y1, int y2, int colour);


// Various functions to display fonts.

// Given coordinates, display a string on the screen
// This fuction will not erase the backgound
// NOTE:pass fontSize as a string like "10x14" for ease of use
void displayText(const char *text, int font_width, int fontColor, int x, int y, const char* fontsize);

// Similar as displayText
// The only difference is that the string will be placed on the center of
// the screen based on the string length. So only the y coordinate is required
void placeTextInCenter(const char *text, int y, int font_width, int fontColour, const char* fontsize);

// The following are fucntions to display a sigle character on the Screen
// When Erase is set to one, the background of the character will be set
// pt backgroundcolour

void OutGraphicsCharFont1(int x, int y, int fontcolour, int backgroundcolour, int c, int Erase);
void OutGraphicsCharFont2(int x, int y, int fontcolour, int backgroundcolour, int c, int Erase);
void OutGraphicsCharFont3(int x, int y, int fontcolour, int backgroundcolour, int c, int Erase);
void OutGraphicsCharFont4(int x, int y, int fontcolour, int backgroundcolour, int c, int Erase);
void OutGraphicsCharFont5(int x, int y, int fontcolour, int backgroundcolour, int c, int Erase);

// This function has an extra argument fontsize, which must be in the form of
// "5x7", "10x14" etc.
void OutGraphicsCharFont(int x, int y, int fontcolour, int backgroundcolour, int c, int Erase, const char *fontsize);

//helper functions
int min(int x, int y);
int max(int x, int y);


#endif /* GRAPHICS_H_ */
