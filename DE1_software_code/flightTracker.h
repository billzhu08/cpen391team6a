#ifndef FLIGHTTRACKER_H_
#define FLIGHTTRACKER_H_

// virtual address of the HW base
// need to be initialized somewhere in main
void* virtual_base;

// Hardware physical address
#define HW_REGS_BASE ( 0xff200000 )
#define HW_REGS_SPAN ( 0x00200000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

// pages that our program can be in
enum State {Menu, Map, List, Following, Credits};

// data structure for storing flight information
typedef struct Flight {
    char *longitude;
    char *latitude;
    int heading; // reserved for map view
    char *date; 
    char *time;
    char *flightID;
} Flight; 

#endif
