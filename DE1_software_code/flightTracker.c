#include "GUI.h"
#include "Graphics.h"
#include "touchscreen.h"
#include "flightTracker.h"
#include "gps.h"
#include "wifi.h"

#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


/*
 * Read the data from "allFlights.txt" and
 * store the information in flights[]
 * The lines in allFlights.txt are
 * flighID
 * time
 * latitude
 * longitude
 */
int readFlight (Flight flights[])
{
    FILE *flightInfo;
    char str[20];
    char* newstr;
    int numFlights = 0;
    int maxFlight = 100;

    /* opening file for reading */
    flightInfo = fopen("allFlights.txt" , "r");
    if( flightInfo == NULL) {
        perror("Error opening file");
        return(-1);
    }
    for (; numFlights < maxFlight; numFlights++) {

        // first field: flight ID
        if( fgets (str, 20, flightInfo)!= NULL ) {
            str[FIDLen] = '\0';
            newstr = malloc(FIDLen + 1);
            if (newstr) {
                // copy the string we just read to newstr
                // beacause we will flush the str soon
                strcpy(newstr,(const char*)str); 
                // set the corresponding pointer in struct Flight 
                flights[numFlights].flightID = newstr;
            }
            else
            {
                return numFlights;
            }
        }
        else {
            return numFlights;
        }
        // second field time
        // same as above
        if( fgets (str, 20, flightInfo)!= NULL ) {
            str[TimeLen] = '\0';
            newstr = malloc(TimeLen + 1);
            if (newstr) {
                strcpy(newstr,(const char*)str);
                flights[numFlights].time = newstr;
            }
            else
            {
                //flightCleanup(&flights[numFlights]);
                return numFlights;
            }
        }
        else {
            //flightCleanup(&flights[numFlights]);
            return numFlights;
        }
        // third field latitude
        if( fgets (str, 20, flightInfo)!= NULL ) {
            str[LatLen] = '\0';
            newstr = malloc(LatLen + 1);
            if (newstr) {
                strcpy(newstr,(const char*)str);
                flights[numFlights].latitude = newstr;
            }
            else
            {
                //flightCleanup(&flights[numFlights]);
                return numFlights;
            }
        }
        else {
            //flightCleanup(&flights[numFlights]);
            return numFlights;
        }
        // last field longitude
        if( fgets (str, 20, flightInfo)!= NULL ) {
            str[LongLen] = '\0';
            newstr = malloc(LongLen + 1);
            if (newstr) {
                strcpy(newstr,(const char*)str);
                flights[numFlights].longitude = newstr;
            }
            else
            {
                //flightCleanup(&flights[numFlights]);
                return numFlights;
            }
        }
        else {
            //flightCleanup(&flights[numFlights]);
            return numFlights; //TODO: CHAGE THIS
        }

    }
    fclose(flightInfo);

    return numFlights;
}

/*
 * Given a point where the user pressed,
 * return 1 if it's on the exit button,
 * return 0 otherwise
 */
int detectExit(Point p) {
    int xLeft = 3700;
    int xRight = 4000;
    int yUp = 0;
    int yLow = 500;

    if (p.x>=xLeft && p.x<=xRight && p.y>=yUp && p.y<=yLow) {
        return 1;
    }
    return 0;
}


/*
 * Display the menu page;
 * Detect touch screen input to return the corresponding
 * state encoding
 */
int menuRoutine()
{
    // upper and lower bounds of each tab in the menu page
    int mapUpper = 700;
    int mapLower = 1200;

	int listUpper = 1200;
  	int listLower = 1700;

    int followingUpper = 1900;
    int followingLower = 2400;

    int creditsUpper = 2400;
    int creditsLower = 3300;

    /* Refreshing data by invoking the wifi functions*/
  	displayMenuPage();

  	WiFiFlush();

    int a = 0;
    while(a<3){
        check();
          printf("sending\n");
        a++;
    }
    postyes();
    sleep(5);
    WiFiFcn();

  	system("python3 parseString.py");

    // Detect the touchscreen input
  	Point p = GetTouched();
    if (p.y <= mapLower && p.y >= mapUpper) { // touch range for map tab
  	    return Map;
  	}
  	if (p.y <= listLower && p.y >= listUpper) { // list tab
  	    return List;
  	}
    if (p.y <= followingLower && p.y >= followingUpper) { // following tab
  	    return Following;
    }
    if (p.y <= creditsLower && p.y >= creditsUpper) { // credits tab
       return Credits;
    }

  	return Menu;
}

/*
 * Display the list page.
 * Press the exit button to return to the menu
 */
int listRoutine()
{
  	Flight flights[100];
  	int numFlights = readFlight(flights);
  	displayListPage(flights, numFlights);

    /*
     * Extract and display the data from GPS
     */
    int lon[10];
  	int lat[11];
  	char latStr[11];
  	char lonStr[10];
  	GPS_Read(&lon[0], &lat[0]);
    // Convert int array to string
  	for (int i=0; i<10; i++) {
  	    latStr[i] = lat[i];
  	}
  	for (int i=0; i<10; i++) {
  	    lonStr[i] = lon[i];
  	}
  	latStr[10] = '\0'; lonStr[9] = '\0';
  	displayText("current location\n",5, WHITE, 430, 17, "5x7");
  	displayText((const char*)latStr, 5, WHITE, 400, 27, "5x7");
  	displayText((const char*)lonStr, 5, WHITE, 500, 27, "5x7");

    //Exit only when the user press the exit button
  	do {
        Point p = GetTouched();
        if (detectExit(p)) return Menu;
    } while (1);
  	return Menu;
}


/*
 * Display the following page.
 * Press anywhere to return to the menu
 */
int followingRoutine()
{
  	Flight flights[100];
  	int numFlights = readFlight(flights);
  	displayFollowPage(flights, numFlights);
  	Point p = GetTouched();
  	return Menu;
}

/*
 * Display the map page.
 * Press anywhere to return to the menu
 */
int mapRoutine()
{
  	Flight flights[100];
  	int numFlights = readFlight(flights);
  	displayMapPage(flights, numFlights);
  	Point p = GetTouched();
  	return Menu;
}

/*
 * Display the about the creadits page.
 * Press anywhere to return to the menu
 */
int CreditsRoutine()
{
    displayCreditsPage();
    Point p = GetTouched();
  	return Menu;
}


/*
 * The main of the flight tracker program, it will
 * initialize each component and than start the GUI
 */
int main (void)
{

  	int fd;
  	// Open memory as if it were a device for read and write access
  	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
        printf( "ERROR: could not open \"/dev/mem\"...\n" );
        return( 1 );
  	}

  	// map 2Mbyte of memory starting at 0xFF200000 to user space
  	virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );

  	if( virtual_base == MAP_FAILED ) {
        printf( "ERROR: mmap() failed...\n" );
        close( fd );
        return(1);
  	}

  	int state = Menu;

	// Init functions
    Init_Touch();
	Init_GPS();
    Init_WiFi();

    //Set up WiFi dongle to execute send_text_message.lua file
    char dofile[] = "dofile(\"send_text_message.lua\")\r\ngpio.mode(3,gpio.OUTPUT)\r\ngpio.write(3,gpio.LOW)\r\n";
    int i = 0;
    //Send set up command to WiFi chip
    while (dofile[i] != '\0') {
        while ((WiFi_LineStatusReg & 0x1) == 0x1) {
            getcharWiFi();
        };
	      int c = dofile[i];
        putcharWiFi(c);
        getcharWiFi();

        while ((WiFi_LineStatusReg & 0x1) == 0x1) {
            getcharWiFi();
        };
        i++;
    };
    //Wait for command to execute and clean the buffer
    sleep(2);
    WiFiFlush();
    //Check WiFi connection
    int a = 0;
    while(a<3){
        check();
        printf("checking wifi...\n");
        a++;
    }
    postyes();        //Send post request
    sleep(5);         //Wait for executing
    WiFiFcn();        //Get and read data from server

  	// Infinite loop
    // For each state, run the corresponding routine function
    // and switch to other states on return
    while (1) {
      switch(state) {
        case Menu:
            state = menuRoutine();
            break;
        case Map:
            state = mapRoutine();
            break;
        case List:
            state = listRoutine();
            break;
        case Following:
            state = followingRoutine();
            break;
        case Credits:
            state = CreditsRoutine();
            break;
        default:
            state = Menu;
            break;
      }
  	}

  	// when finished, unmap the virtual space and close the memory ï¿½deviceï¿½
    if( munmap( virtual_base, HW_REGS_SPAN ) != 0 ) {
        printf( "ERROR: munmap() failed...\n" );
        close( fd );
        return( 1 );
  	}
  	close( fd );
  	return 0;
}
