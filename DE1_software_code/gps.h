#ifndef GPS_H_
#define GPS_H_

#include <stdio.h>
#include "flightTracker.h"

//#define GPS_ReceiverFifo (*(volatile unsigned char *)(0xFF210210))
#define GPS_ReceiverFifo (*(volatile unsigned char *) ((virtual_base + ((0x10210 ) &  (HW_REGS_MASK )))))
//#define GPS_TransmitterFifo (*(volatile unsigned char *)(0xFF210210))
#define GPS_TransmitterFifo (*(volatile unsigned char *) ((virtual_base + ((0x10210 ) &  (HW_REGS_MASK )))))
//#define GPS_InterruptEnableReg (*(volatile unsigned char *)(0xFF210212))
#define GPS_InterruptEnableReg (*(volatile unsigned char *) ((virtual_base + ((0x10212 ) &  (HW_REGS_MASK )))))
//#define GPS_InterruptIdentificationReg (*(volatile unsigned char *)(0xFF210214))
#define GPS_InterruptIdentificationReg (*(volatile unsigned char *) ((virtual_base + ((0x10214 ) &  (HW_REGS_MASK )))))
//#define GPS_FifoControlReg (*(volatile unsigned char *)(0xFF210214))
#define GPS_FifoControlReg (*(volatile unsigned char *) ((virtual_base + ((0x10214 ) &  (HW_REGS_MASK )))))
//#define GPS_LineControlReg (*(volatile unsigned char *)(0xFF210216))
#define GPS_LineControlReg (*(volatile unsigned char *) ((virtual_base + ((0x10216 ) &  (HW_REGS_MASK )))))
//#define GPS_ModemControlReg (*(volatile unsigned char *)(0xFF210218))
#define GPS_ModemControlReg (*(volatile unsigned char *) ((virtual_base + ((0x10218 ) &  (HW_REGS_MASK )))))
//#define GPS_LineStatusReg (*(volatile unsigned char *)(0xFF21021A))
#define GPS_LineStatusReg (*(volatile unsigned char *) ((virtual_base + ((0x1021A ) &  (HW_REGS_MASK )))))
//#define GPS_ModemStatusReg (*(volatile unsigned char *)(0xFF21021C))
#define GPS_ModemStatusReg (*(volatile unsigned char *) ((virtual_base + ((0x1021C ) &  (HW_REGS_MASK )))))
//#define GPS_ScratchReg (*(volatile unsigned char *)(0xFF21021E))
#define GPS_ScratchReg (*(volatile unsigned char *) ((virtual_base + ((0x1021E ) &  (HW_REGS_MASK )))))
//#define GPS_DivisorLatchLSB (*(volatile unsigned char *)(0xFF210210))
#define GPS_DivisorLatchLSB (*(volatile unsigned char *) ((virtual_base + ((0x10210 ) &  (HW_REGS_MASK )))))
//#define GPS_DivisorLatchMSB (*(volatile unsigned char *)(0xFF210212))
#define GPS_DivisorLatchMSB (*(volatile unsigned char *) ((virtual_base + ((0x10212 ) &  (HW_REGS_MASK )))))

//#define HEX0_1 (*(volatile unsigned char *)(0xFF200030))
#define HEX0_1 (*(volatile unsigned char *) ((virtual_base + ((0x30 ) &  (HW_REGS_MASK )))))
//#define HEX2_3 (*(volatile unsigned char *)(0xFF200040))
#define HEX2_3 (*(volatile unsigned char *) ((virtual_base + ((0x40 ) &  (HW_REGS_MASK )))))
//#define HEX4_5 (*(volatile unsigned char *)(0xFF200050))
#define HEX4_5 (*(volatile unsigned char *) ((virtual_base + ((0x50 ) &  (HW_REGS_MASK )))))

void GPS_Read(int *lon,int *lat);
int parse(int *c,int *lon, int *lat);
int getcharGPS( void );
void Init_GPS(void);
int putcharGPS(int c);
void GPS_Write (void);
int GPSTestForReceivedData(void);
void GPSFlush(void);

#endif
