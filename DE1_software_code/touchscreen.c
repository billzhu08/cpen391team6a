#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "touchscreen.h"


//set up the Macro to print out the coordinates of touch screen to debug
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c\n"
#define BYTE_TO_BINARY(byte)       \
    (byte & 0x80 ? '1' : '0'),     \
        (byte & 0x40 ? '1' : '0'), \
        (byte & 0x20 ? '1' : '0'), \
        (byte & 0x10 ? '1' : '0'), \
        (byte & 0x08 ? '1' : '0'), \
        (byte & 0x04 ? '1' : '0'), \
        (byte & 0x02 ? '1' : '0'), \
        (byte & 0x01 ? '1' : '0')


//initialize the touch screen 
void Init_Touchscreen(void)
{
    // set bit 7 of Line Control Register to 1, to gain access to the baud rate registers
    Touchscreen_LineControlReg = 0x80;
    // set Divisor latch (LSB and MSB) with correct value for required baud rate
    Touchscreen_DivisorLatchLSB = 0x45;
    Touchscreen_DivisorLatchMSB = 0x01;
    // set bit 7 of Line control register back to 0 and
    // program other bits in that reg for 8 bit data, 1 stop bit, no parity etc
    Touchscreen_LineControlReg = 0x03;
    // Reset the Fifo’s in the FiFo Control Reg by setting bits 1 & 2
    Touchscreen_FifoControlReg = 0x06;
    // Now Clear all bits in the FiFo control registers
    Touchscreen_FifoControlReg = 0x00;
}


//write data into the buffer
int putcharTouchscreen(int c)
{
    // wait for Transmitter Holding Register bit (5) of line status register to be '1'
    // indicating we can write to the device
    while ((Touchscreen_LineStatusReg & 0x20) != 0x20)
    {
    };
    // write character to Transmitter fifo register
    Touchscreen_TransmitterFifo = c;
    // return the character we printed
    return c;
}

//read date from the buffer
int getcharTouchscreen(void)
{
    // wait for Data Ready bit (0) of line status register to be '1'
    while ((Touchscreen_LineStatusReg & 0x1) == 0);
    // read new character from ReceiverFiFo register
    int c = Touchscreen_ReceiverFifo;
    // return new character
    return c;
}

// the following function polls the UART to determine if any character
// has been received. It doesn't wait for one, or read it, it simply tests
// to see if one is available to read from the FIFO
int TouchscreenTestForReceivedData(void)
{
    // if Touchscreen_LineStatusReg bit 0 is set to 1
    if ((Touchscreen_LineStatusReg & 0x1) == 0x1)
        return 1;
    //return TRUE, otherwise return FALSE
    return 0;
}

//
// Remove/flush the UART receiver buffer by removing any unread characters
//
void TouchscreenFlush(void)
{
    // while bit 0 of Line Status Register == ‘1’
    while ((Touchscreen_LineStatusReg & 0x1) == 0x1)
    {
        getcharTouchscreen();
    };
    // read unwanted char out of fifo receiver buffer
    return;
    // return; // no more characters so return
}

/*****************************************************************************
**  Initialise touch screen controller
*****************************************************************************/
void Init_Touch(void)
{
    // Program UART and baud rate generator to communicate with touchscreen
    // use similar params used for serial port 9600 baud,8 bits data, no parity,1 stop bit etc.
    Init_Touchscreen();
    // Send touchscreen controller an "enable touch" command hex 55, 01, 12
    putcharTouchscreen(0x55);
    putcharTouchscreen(0x01);
    putcharTouchscreen(0x12);
}

/*****************************************************************************
**   test if screen is simply touched (regardless of event type)
*****************************************************************************/
int ScreenTouched(void)
{
    TouchscreenFlush();
    // return TRUE if touch screen controller producing any data
    return ((Touchscreen_LineStatusReg & 0x1) != 0);
}

/*****************************************************************************
**   wait for screen to be touched
*****************************************************************************/
void WaitForTouch()
{

    while (!ScreenTouched());
    printf("Touched!!!\n");

}

/*****************************************************************************
* This function waits for a touch screen press event and returns X,Y coord
*****************************************************************************/
Point GetPress(void)
{
    Point p1;

    int byte, x, x1, x2, y, y1, y2;

    byte = getcharTouchscreen();
    while (((byte & 0x1) != 0x1) || ((byte & 0x80) != 0x80)) byte = getcharTouchscreen();
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    byte = getcharTouchscreen();
    x1 = byte;
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    byte = getcharTouchscreen();
    x2 = byte;
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    byte = getcharTouchscreen();
    y1 = byte;
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    byte = getcharTouchscreen();
    y2 = byte;
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    x = x1 + x2 * 128;
    y = y1 + y2 * 128;

    printf("(x: %d , y: %d is pressed)\n", x, y);

    // calibrated correctly so that it maps to a pixel on screen
    p1.x = x;
    p1.y = y;

    return p1;
}


/*****************************************************************************
* This function waits for a touch screen release event and returns X,Y coord
*****************************************************************************/
Point GetRelease(void)
{
    Point p1;

    int byte, x, x1, x2, y, y1, y2;

    byte = getcharTouchscreen();
    while (((byte & 0x1) != 0x0) || ((byte & 0x80) != 0x80)) byte = getcharTouchscreen();
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    byte = getcharTouchscreen();
    x1 = byte;
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    byte = getcharTouchscreen();
    x2 = byte;
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    byte = getcharTouchscreen();
    y1 = byte;
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    byte = getcharTouchscreen();
    y2 = byte;
    //printf("Data: " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));

    x = x1 + x2 * 128;
    y = y1 + y2 * 128;

    printf("(x: %d , y: %d is released)\n", x, y);
    TouchscreenFlush();

    // calibrated correctly so that it maps to a pixel on screen
    p1.x = x;
    p1.y = y;

    return p1;
}



/*****************************************************************************
* This function waits for a touch screen including both press and release events 
* and returns X,Y coord
*****************************************************************************/
Point GetTouched(void) {
    Point p1, p2;

    p1 = GetPress();
    p2 = GetRelease();


    int x = p2.x;
    int y = p2.y; 

    printf("(x: %d , y: %d is touched)\n", x, y);
    return p2;
}
