#include <stdio.h>
#include <stdlib.h>

#include "wifi.h"


void Init_WiFi(void)
{
    // set bit 7 of Line Control Register to 1, to gain access to the baud rate registers
    WiFi_LineControlReg = 0x80;
    // set Divisor latch (LSB and MSB) with correct value for required baud rate
    WiFi_DivisorLatchLSB = (50000000/(115200*16))%256;
    WiFi_DivisorLatchMSB = (50000000/(115200*16))/256;
         //printf("%d,%d\n", WiFi_DivisorLatchLSB, WiFi_DivisorLatchMSB);
    // set bit 7 of Line control register back to 0 and
    // program other bits in that reg for 8 bit data, 1 stop bit, no parity etc
    WiFi_LineControlReg = 0x07;
    // Reset the Fifos in the FiFo Control Reg by setting bits 1 & 2
    WiFi_FifoControlReg = 0x06;
    // Now Clear all bits in the FiFo control registers
    WiFi_FifoControlReg = 0x00;

}

int putcharWiFi(int c)
{
    // wait for Transmitter Holding Register bit (5) of line status register to be '1'
    // indicating we can write to the device
    while ((WiFi_LineStatusReg & 0x20) != 0x20) {
                 //printf("bus\n");
                 };
    // write character to Transmitter fifo register
                 //printf("%c%c",c/256,c%256);
    WiFi_TransmitterFifo = c;
    // return the character we printed
    return c;

}

int getcharWiFi( void )
{
    // wait for Data Ready bit (0) of line status register to be '1'
    while ((WiFi_LineStatusReg & 0x1) != 0x1) {};
    // read new character from ReceiverFiFo register
    int c = WiFi_ReceiverFifo;
    // return new character
    return c;
}

// the following function polls the UART to determine if any character
// has been received. It doesn't wait for one, or read it, it simply tests
// to see if one is available to read from the FIFO
int WiFiTestForReceivedData(void)
{
    // if WiFi_LineStatusReg bit 0 is set to 1
    if ((WiFi_LineStatusReg & 0x1) == 0x1) return 1;
    //return TRUE, otherwise return FALSE
    return 0;
}

//
// Remove/flush the UART receiver buffer by removing any unread characters
//
void WiFiFlush(void)
{
    // while bit 0 of Line Status Register == 1
    while ((WiFi_LineStatusReg & 0x1) == 0x1) {
                 int a = getcharWiFi();
                 };
    // read unwanted char out of fifo receiver buffer
    return;
    // return; // no more characters so return
}
//Send commands to get and read data back from WiFi dongle
void WiFiFcn(void)
{
    //Lua commands
	char check[] = "getData()\r\n";
	char read[] = "readData()\r\n";
	int i = 0;             //loop index for sending commands
	int j = 0;             //loop index for receiving data
	int res[204800];       //buffer for receiving data
	int a = 0;             //counter for waiting getcharWiFi()

    //Sending "getData()" command
	i = 0;
	while (check[i] != '\0') {
        while ((WiFi_LineStatusReg & 0x1) == 0x1) {
			getcharWiFi();
		};

		int c = check[i];
		putcharWiFi(c);
        getcharWiFi();

		i++;
	};
    //Continue receiving data until all data received
	a = 0;
	while (a < 600000) {
		while ((WiFi_LineStatusReg & 0x1) == 0x1) {
			getcharWiFi();
		};
		a++;
	}

    //Wait and clean buffer
	sleep(3);
    WiFiFlush();
    
    //Sending "readData()" command
	i = 0;
	while (read[i] != '\0') {
		while ((WiFi_LineStatusReg & 0x1) == 0x1) {
			getcharWiFi();
		};

		int c = read[i];
		putcharWiFi(c);
        getcharWiFi();
        
		i++;
	};
    //Continue receiving data until all data received
	a = 0;
	while (a < 600000) {
		while ((WiFi_LineStatusReg & 0x1) == 0x1) {
            res[j] = getcharWiFi();
			j++;
		};
		a++;
	}
    //Append char to indicate ending
	res[j] = '\0';

    //Save and print data in the data receive buffer
    if(res[0] == '['){
	    remove("data.txt");
	    FILE *fptr = fopen("data.txt", "w");
	    if (fptr == NULL)
    	{
		    printf("Error to create file!");
	    	exit(1);
     	}

	    j = 0;
	    printf("Result get:\n");
	    while (res[j] != '\0')
    	{
		    printf("%c", res[j]);
		    fprintf(fptr, "%c", res[j]);
	    	j++;
	    }
	    fclose(fptr);
    }else{
        printf("No data recieved.");
    }
    //Flush WiFi dongle and return function
	printf("\n");
    WiFiFlush();
	return;
}

//Send POST request to server
void postyes(void)
{
    char post[] = "post()\r\n";
	int i = 0;
	int j = 0;

    while (post[i] != '\0') {
		WiFiFlush();
		int c = post[i];
		putcharWiFi(c);

		while ((WiFi_LineStatusReg & 0x1) == 0x1) {
			getcharWiFi();
		};

		i++;
	};

    WiFiFlush();
	sleep(2);
    return;
}

//Check WiFi connection status by sending text to mobile
void check(void)
{
	char check[] = "check_wifi()\r\n";
	int i = 0;
	int j = 0;

	while(check[i] != '\0'){
		WiFiFlush();
		int c = check[i];
		putcharWiFi(c);

        while((WiFi_LineStatusReg & 0x1) == 0x1){
			getcharWiFi();
		}
		i++;
	}
	
	WiFiFlush();
	sleep(2);
	return;
}
