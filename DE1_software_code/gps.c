#include "gps.h"

void Init_GPS(void)
{
    // set bit 7 of Line Control Register to 1, to gain access to the baud rate registers 
    GPS_LineControlReg = 0x80;
    // set Divisor latch (LSB and MSB) with correct value for required baud rate
    GPS_DivisorLatchLSB = 0x45;//0x45  0x0145 for 9600 50M/16/325
    GPS_DivisorLatchMSB = 0x01;//0x01
    // set bit 7 of Line control register back to 0 and
    // program other bits in that reg for 8 bit data, 1 stop bit, no parity etc
    GPS_LineControlReg = 0x03; 
    // Reset the Fifo’s in the FiFo Control Reg by setting bits 1 & 2
    GPS_FifoControlReg = 0x06;
    // Now Clear all bits in the FiFo control registers
    GPS_FifoControlReg = 0x00;
}

int putcharGPS(int c) // change int to char here
{
    // wait for Transmitter Holding Register bit (5) of line status register to be '1' 
    // indicating we can write to the device
    while ((GPS_LineStatusReg & 0x20) != 0x20) {};
    // write character to Transmitter fifo register
    GPS_TransmitterFifo = c;
    // return the character we printed
    return c;

}

void GPS_Write (void){
    char erase [] = "$PMTK184*22\r\n";
    char start[] = "$PMTK185,0*23\r\n";
    char snap[] = "$PMTK186,1*20\r\n";
    char stop[] = "$PMTK185,1*23\r\n";
    char cmd [] =   "$PMTK622,0*28\r\n"; 
   // int c;
    int i;
    int j;
    int k;
    int a = 0;
    for (j = 0; j < 14; j = j + 2){
       putcharGPS((erase[j]<< 8+ erase[j+1]));
    }
    for (k = 0; k < 16; k = k+2){
        putcharGPS((start[k]<<8+start[k+1]));
    }
    
    for(k = 0; k < 16; k = k+2){
        putcharGPS((snap[k]<<8+snap[k+1]));
    }

    for(k = 0; k < 16; k = k+2){
        putcharGPS((stop[k]<<8+stop[k+1]));
    }
   
    for(i = 0; i < 16; i = i + 2){
       putcharGPS((cmd[i]<< 8+ cmd[i+1]));
    }


      
}

int getcharGPS( void )
{
    // wait for Data Ready bit (0) of line status register to be '1'
    while ((GPS_LineStatusReg & 0x1) != 0x1) {};
    // read new character from ReceiverFiFo register
    int c = GPS_ReceiverFifo;
    // return new character
    return c;
}

int parse(int *c,int* lon, int *lat){
    int i = 0;
    if((char) c[i] == '$' && (char)c[i+1] == 'G' && (char)c[i+2] == 'P' && (char)c[i+3] == 'G'  && (char)c[i+4] == 'G'  && (char)c[i+5] == 'A' ){
        for(i = 0; i < 9; i++){
            lon[i] = c[18+i];
            lat[i] = c[30+i];
        }
        lat[9] = c[39];
        return 1;

    }
    return 0;
    
}

void GPS_Read(int *lon, int *lat){
    int c [5000];
    int i = 0;
    int j = 0;
    int k = 0;
    int ret = 0;
  
    while(k <14999){
      c[i] = getcharGPS();
      if(c[i] == '\n'){
          ret = parse(c,lon,lat);
          i = 0;
          if(ret == 1){
              break;
          }
          continue;
      }
       k++;
       i++;
    }
   
}





// the following function polls the UART to determine if any character
// has been received. It doesn't wait for one, or read it, it simply tests
// to see if one is available to read from the FIFO
int GPSTestForReceivedData(void)
{
    // if RS232_LineStatusReg bit 0 is set to 1
    if ((GPS_LineStatusReg & 0x1) == 0x1) return 1;
    //return TRUE, otherwise return FALSE 
    return 0;
}

//
// Remove/flush the UART receiver buffer by removing any unread characters
//
void GPSFlush(void)
{
    // while bit 0 of Line Status Register == ‘1’
    // read unwanted char out of fifo receiver buffer
    while ((GPS_LineStatusReg & 0x1) == 0x1) {
        getcharGPS();
    };
    
    return;
    // return; // no more characters so return
}



