#ifndef WIFI_H_
#define WIFI_H_

#include "flightTracker.h"

//#define WiFi_ReceiverFifo (*(volatile unsigned char *)(0xFF210200))
#define WiFi_ReceiverFifo (*(volatile unsigned char *) ((virtual_base + ((0x10200 ) &  (HW_REGS_MASK )))))

//#define WiFi_TransmitterFifo (*(volatile unsigned char *)(0xFF210200))
#define WiFi_TransmitterFifo (*(volatile unsigned char *) ((virtual_base + ((0x10200 ) &  (HW_REGS_MASK )))))

//#define WiFi_InterruptEnableReg (*(volatile unsigned char *)(0xFF210202))
#define WiFi_InterruptEnableReg (*(volatile unsigned char *) ((virtual_base + ((0x10202 ) &  (HW_REGS_MASK )))))

//#define WiFi_InterruptIdentificationReg (*(volatile unsigned char *)(0xFF210204))
#define WiFi_InterruptIdentificationReg (*(volatile unsigned char *) ((virtual_base + ((0x10204 ) &  (HW_REGS_MASK )))))

//#define WiFi_FifoControlReg (*(volatile unsigned char *)(0xFF210204))
#define WiFi_FifoControlReg (*(volatile unsigned char *) ((virtual_base + ((0x10204 ) &  (HW_REGS_MASK )))))

//#define WiFi_LineControlReg (*(volatile unsigned char *)(0xFF210206))
#define WiFi_LineControlReg (*(volatile unsigned char *)((virtual_base + ((0x10206 ) &  (HW_REGS_MASK )))))

//#define WiFi_ModemControlReg (*(volatile unsigned char *)(0xFF210208))
#define WiFi_ModemControlReg (*(volatile unsigned char *) ((virtual_base + ((0x10208 ) &  (HW_REGS_MASK )))))

//#define WiFi_LineStatusReg (*(volatile unsigned char *)(0xFF21020A))
#define WiFi_LineStatusReg (*(volatile unsigned char *)((virtual_base + ((0x1020A ) &  (HW_REGS_MASK )))))

//#define WiFi_ModemStatusReg (*(volatile unsigned char *)(0xFF21020C))
#define WiFi_ModemStatusReg (*(volatile unsigned char *) ((virtual_base + ((0x1020C ) &  (HW_REGS_MASK )))))

//#define WiFi_ScratchReg (*(volatile unsigned char *)(0xFF21020E))
#define WiFi_ScratchReg (*(volatile unsigned char *) ((virtual_base + ((0x1020E ) &  (HW_REGS_MASK )))))

//#define WiFi_DivisorLatchLSB (*(volatile unsigned char *)(0xFF210200))
#define WiFi_DivisorLatchLSB (*(volatile unsigned char *)((virtual_base + ((0x10200) &  (HW_REGS_MASK )))))

//#define WiFi_DivisorLatchMSB (*(volatile unsigned char *)(0xFF210202))
#define WiFi_DivisorLatchMSB (*(volatile unsigned char *)((virtual_base + ((0x10202) &  (HW_REGS_MASK )))))


void postyes(void);
void Test(void);
void WiFiFcn(void);
void WiFiFlush(void);
int WiFiTestForReceivedData(void);
int getcharWiFi( void );
int putcharWiFi(int c);
void Init_WiFi(void);
void check(void);

#endif
