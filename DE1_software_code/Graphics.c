#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Graphics.h"

/*******************************************************************************************
* This function writes a single pixel to the x,y coords specified using the specified colour
* Note colour is a byte and represents a palette number (0-255) not a 24 bit RGB value
********************************************************************************************/
void WriteAPixel(int x, int y, int Colour)
{
	WAIT_FOR_GRAPHICS;				// is graphics ready for new command

	GraphicsX1Reg = x;				// write coords to x1, y1
	GraphicsY1Reg = y;
	GraphicsColourReg = Colour;			// set pixel colour
	GraphicsCommandReg = PutAPixel;			// give graphics "write pixel" command
}

/*********************************************************************************************
* This function read a single pixel from the x,y coords specified and returns its colour
* Note returned colour is a byte and represents a palette number (0-255) not a 24 bit RGB value
*********************************************************************************************/

int ReadAPixel(int x, int y)
{
	WAIT_FOR_GRAPHICS;			// is graphics ready for new command

	GraphicsX1Reg = x;			// write coords to x1, y1
	GraphicsY1Reg = y;
	GraphicsCommandReg = GetAPixel;		// give graphics a "get pixel" command

	WAIT_FOR_GRAPHICS;			// is graphics done reading pixel
	return (int)(GraphicsColourReg) ;	// return the palette number (colour)
}


/**********************************************************************************
** subroutine to program a hardware (graphics chip) palette number with an RGB value
** e.g. ProgramPalette(RED, 0x00FF0000) ;
**
************************************************************************************/

void ProgramPalette(int PaletteNumber, int RGB)
{
	WAIT_FOR_GRAPHICS;
	GraphicsColourReg = PaletteNumber;
	GraphicsX1Reg = RGB >> 16   ;        // program red value in ls.8 bit of X1 reg
	GraphicsY1Reg = RGB ;                // program green and blue into ls 16 bit of Y1 reg
	GraphicsCommandReg = ProgramPaletteColour; // issue command
}

/**************************************************************************************************************
** This function writes to graphics controller registers and calls the hardware horzontal line implementation
***************************************************************************************************************/
void drawHLine(int x1, int x2, int y1, int colour)
{
	WAIT_FOR_GRAPHICS;

	int temp;

	// check that all values are valid
	if(x1 < 0 || x2 < 0 || y1 < 0 || x1 > 800 || x2 > 800 || y1 > 480 || colour < 0 || colour > 255)
	{
		printf("invalid? \n");
		return;
	}


	if(x1 > x2)
	{
		temp = x1;
		x1 = x2;
		x2 = temp;
	}
	GraphicsX1Reg = x1;
	GraphicsX2Reg = x2;
	GraphicsY1Reg = y1;
	GraphicsColourReg = colour;
	GraphicsCommandReg = DrawHLine;
}

/**************************************************************************************************************
** This function writes to graphics controller registers then calls the hardware vertical line implementation
***************************************************************************************************************/
void drawVLine(int x1, int y1, int y2, int colour)
{
	WAIT_FOR_GRAPHICS;

	int temp;

	//	// check that all values are valid
	if(x1 < 0 || y1 < 0 || y2 < 0 || x1 > 800 || y1 > 480 || y2 > 480 || colour < 0 || colour > 255)
	{
		printf("invalid? \n");
		return;
	}

	// swap values if y1 is bigger than y2
	if(y1 > y2)
	{
		temp = y1;
		y1 = y2;
		y2 = temp;
	}

	//printf("Sending: x1: %d, y1: %d, y2: %d, colour: %d\n", x1, y1, y2, colour);
	// write to the graphics controller registers
	GraphicsX1Reg = x1;
	GraphicsY1Reg = y1;
	GraphicsY2Reg = y2;
	GraphicsColourReg = colour;
	GraphicsCommandReg = DrawVLine;
}

/**************************************************************************************************************
** This function writes to graphics controller registers and calls the hardware line-drawing implementation
***************************************************************************************************************/
void drawLine(int x1, int x2, int y1, int y2, int colour)
{
	WAIT_FOR_GRAPHICS;

	int temp;

	// check that all x and colour values are valid
	if(x1 < 0 || x2 < 0 || x1 > 799 || x2 > 799 || colour < 0 || colour > 255)
	{
		return;
	}
	// check that all y values are valid
	if(y1 < 0 || y2 < 0 || y1 > 479 || y2 > 479)
	{
		return;
	}

	// handle vertical or horizoltal lines separatly
	if(y1 == y2) {
		drawHLine(x1, x2, y1, colour);
	}
	if (x1 == x2)
	{
		drawVLine(x1, y1, y2, colour);
	}

	// the hardware implementation assumes x1 < x2
	if(x1 > x2)
	{
		temp = x1;
		x1 = x2;
		x2 = temp;

		temp = y1;
		y1 = y2;
		y2 = temp;
	}

	GraphicsX1Reg = x1;
	GraphicsX2Reg = x2;
	GraphicsY1Reg = y1;
	GraphicsY2Reg = y2;
	GraphicsColourReg = colour;
	GraphicsCommandReg = DrawLine;
}

/**************************************************************************************************************
** Calls the hardware rectangle-drawing implementation
***************************************************************************************************************/
// helper function
int min(int x, int y)
{
	if(x < y)
	{
		return x;
	}
	return y;
}

// helper function for drawRectangle
int max(int x, int y)
{
	if(x < y)
	{
		return y;
	}
	return x;
}

// (x,y) is the top right corner
void drawRectangle(int x, int y, int width, int height, int colour)
{
	WAIT_FOR_GRAPHICS;

	// check that parameters are valid
	if(x < 0 || y < 0 || colour < 0 || x >= 799 || y >= 479 || colour > 255)
	{
		printf("Invalid parameter passed to drawRectangle\n");
		return;
	}

	//printf("Sending: x: %d, x2: %d, y: %d, height: %d, colour: %d\n", x, (x+width), y, height, colour);
	// write to graphics controller registers
	GraphicsX1Reg = x;
	GraphicsX2Reg = min((x + width), 799);
	GraphicsY1Reg = y;
	GraphicsY2Reg = min((y + height), 479);
	GraphicsColourReg = colour;
	GraphicsCommandReg = DrawRectangle;
}

// make the whole screen the same colour
void clearScreen(int colour)
{
	int x1 = 0;
	int width = display_cols;
	int y1 = 0;
	int height = display_rows;

	drawRectangle(x1, y1, width, height, colour);

}

// draw a triangle
// (x,y) are the top point, height is the height of the triangle
void drawTriangle(int x, int y, int height, int colour)
{
	int xLeft = x - height/2;
	int xRight = x + height/2;
	int yLower = y - height;
	drawLine(xLeft, x, yLower, y, colour);
	drawLine(xLeft, x, y, yLower, colour);
	drawLine(xLeft, xRight, yLower, yLower, colour);

}

// Similar to drawRectangle, but with 4 borders of colour defined by "boarder"
void drawRectangleBoarder(int x, int y, int width, int height, int colour, int boarder){
	int i;
	for(i = 0; i < 3; i++){
		// top boarder
		drawHLine(x, min((x + width), 799), y, boarder);

		// bottom boarder
		drawHLine(x, min((x + width), 799), min((y + height), 479), boarder);

		// right boarder
		drawVLine(x, y, min((y + height), 479), boarder);

		//left boarder
		drawVLine(min((x + width), 799), y, min((y + height), 479), boarder);

		x++;
		y++;
		height = height - 2;
		width = width - 2;
	}
	drawRectangle(x, y, width, height, colour);
}


// Display a character with certain fontsize, such as "5x7"...
void OutGraphicsCharFont(int x, int y, int fontcolour, int backgroundcolour, int c, int Erase, const char *fontsize)
{
	// Call the corresponding font fuctions based on the fontSize
	if (strcmp(fontsize, "5x7") == 0) {
		OutGraphicsCharFont1(x, y, fontcolour, backgroundcolour, c, Erase);
	}
	else if (strcmp(fontsize, "10x14") == 0) {
		OutGraphicsCharFont2(x, y, fontcolour, backgroundcolour, c, Erase);

	} else if (strcmp(fontsize, "16x27") == 0) {
		OutGraphicsCharFont3(x, y, fontcolour, backgroundcolour, c, Erase);

	} else if (strcmp(fontsize, "22x40") == 0) {
		OutGraphicsCharFont4(x, y, fontcolour, backgroundcolour, c, Erase);

	} else if (strcmp(fontsize, "38x59") == 0) {
		OutGraphicsCharFont5(x, y, fontcolour, backgroundcolour, c, Erase);
	}
}

// Given coordinates, display a string on the screen
// This fuction will not erase the backgound
// NOTE:pass fontSize as a string like "10x14" for ease of use
void displayText(const char *text, int font_width, int fontColor, int x, int y, const char* fontsize)
{
    const char *c = text;
    while (*c != '\0') {
        printf("%c", *c);
        OutGraphicsCharFont(x, y, fontColor, WHITE, (int)*c, 0, fontsize);
        c++; // move to next char
        x+=font_width; // move x coordinate to the right by fontWidth
    }
}

// Similar as displayText
// The only difference is that the string will be placed on the center of
// the screen based on the string length. So only the y coordinate is required
void placeTextInCenter(const char *text, int y, int font_width, int fontColour, const char* fontsize)
{
	int textLen = strlen(text);
	// maximum # of characters that can fit in the screen given font_width
	int maxDisplay = display_cols / font_width;

	int charIndex = (maxDisplay / 2) - (textLen / 2);
	int x = charIndex * font_width; // calculated starting index of the string

	displayText(text, font_width, fontColour, x, y, fontsize);

}
