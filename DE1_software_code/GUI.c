#include "GUI.h"
#include "Graphics.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>


/************************************************************************************************
** This file contains the functions to display all of our GUI pages
** The desired output of each function is specified in the high level design document
***********************************************************************************************/

/*
 * The following functions are supposed to be
 * completed by Courtney. We have to comment them
 * out because they do not compile
 */

/*// function to display menu page
void displayMenuPage()
{
        drawMap();
        drawButton(10, 40, 10, 30, "BACK", RED, BLACK);

}

void displayMapPage(Flight* flights, int numFlights)
{
        int i;
        // draw background
        drawMap();

        // draw back button
        drawButton(10, 40, 10, 30, "BACK", RED, BLACK);

        // draw planes
        for(i = 0; i < numFlights; i++)
        {
                drawAllPlanes(flights, numFlights);
        }
}

// function to draw a plane at the location given
void drawAllPlanes(Flight flights[], int numFlights)
{
        for(int i = 0; i < numFlights; i++)
        {
                drawPlane(flights[i]);
        }
}

void drawPlane(Flight flight)
{
        int x, y;
        int latitude = flight.latitude;
        int longitude = flight.longitude;

        // test if the plane is in the range that we can display on the screen
        if(latitude < MIN_LATITUDE || latitude > MAX_LATITUDE || longitude < MIN_LONGITUDE || longitude > MAX_LONGITUDE)
        {
                return;
        }

        x = MAX_LONGITUDE - longitude/(MAX_LONGITUDE - MIN_LATITUDE);
        y = MAX_LATITUDE - latitude/(MAX_LATITUDE - MIN_LATITUDE);

}*/


//draw a rectangular button of fixed size with some text.
void drawButton (char* text, int x, int y, int backgroundColour, int fontColour)
{
    char* fontSize = "10x14";
    int fontWidth = 10;
    drawRectangleBoarder(x, y, 60, 30, backgroundColour, fontColour);
    displayText(text, fontWidth, fontColour, x+8, y+8, fontSize);
}


// Draw a square exit button, position and size are fixed
void drawExitButton ()
{
    int x = 750;
    int y = 10;
    int width = 25;
    int xRight = x + width;
    int yBottom = y + width;

    //Draw the boarders: top, bottom, left, right
    drawLine(x, xRight, y, y, WHITE);
    drawLine(x, xRight, yBottom, yBottom, WHITE);
    drawLine(x, x, y, yBottom, WHITE);
    drawLine(xRight, xRight, y, yBottom, WHITE);

    //Draw the diagonal lines
    drawLine(x, xRight, y, yBottom, WHITE);
    drawLine(x, xRight, yBottom, y, WHITE);

}

// Draw the header of our program
void drawHeader()
{
  	int leftMargin = 45;
  	int topMargin = 5;

  	displayText("FLIGHT TRACKER", 22, WHITE, leftMargin, topMargin, "22x40");
}

// Function to display the listpage
// numFlights is the number of flights available in flights[] array
void displayListPage(Flight flights[], int numFlights)
{
    //draw the backgroud
    clearScreen(BLACK); //Not working now...

    int leftMargin = 45;
    int topMargin  = 50;

    //FLIGHT TRACKER header
    drawHeader();

    //tab background
    int tabWidth = 240;
    int tabHeight= 50;
    int boarder = 2;
    drawRectangleBoarder(leftMargin, topMargin, tabWidth, tabHeight, YELLOW, boarder);
    drawRectangleBoarder(leftMargin+tabWidth+boarder, topMargin, tabWidth, tabHeight, RED, boarder);
    drawRectangleBoarder(leftMargin+2*tabWidth+2*boarder, topMargin, tabWidth, tabHeight, RED, boarder);

    //tab text
    displayText("List", 16, BLACK, 90, 70, "16x27");
    displayText("Following", 16, BLACK, 300, 70, "16x27");
    displayText("Search", 16, BLACK, 560, 70, "16x27");


    //three rows of data
    int rowWidth = 722;
    int rowHeight = 120;
    topMargin += tabHeight;
    drawRectangle(leftMargin, topMargin, rowWidth, rowHeight, WHITE);
    drawRectangle(leftMargin, topMargin+rowHeight, rowWidth, rowHeight, GREY);
    drawRectangle(leftMargin, topMargin+2*rowHeight, rowWidth, rowHeight, WHITE);

    //the exit drawButton
    drawExitButton();

    //display the flight info
    int longIndex = 18;
    int latIndex = 39;
    int timeIndex = 66;
    //format of how the data should be displayed
    // we will change the content of text[]
    char text[80] = "AC1234       Long:49.43            Lat:84.345        Last Update: 12:42\n";
    numFlights = min(numFlights, 3); // we can only display up to 3 flights in one page

    //for each of the three flights, copy the data to text[]
    for (int i=0; i<numFlights; i++) {
        for (int j=0; j<FIDLen; j++) {
            text[j] = flights[i].flightID[j];
        }
        for (int j=0; j<LongLen; j++) {
            text[j+longIndex] = flights[i].longitude[j];
        }
        for (int j=0; j<LatLen; j++) {
            text[j+latIndex] = flights[i].latitude[j];
        }
        for (int j=0; j<TimeLen; j++) { //hour:min
            text[j+timeIndex] = flights[i].time[j];
        }
        // Except the last one, convert all '\0's to spaces
        // to make it into one string
        for (int j=0; j<79; j++) {
            if (text[j] == '\0')
                text[j] = ' ';
        }
        displayText(text, 10, BLACK, leftMargin+10, 130+120*i, "10x14");
    }
}

// Function to display the menu page
void displayMenuPage()
{
  	int leftMargin = 45;
  	int topMargin  = 45;
  	int rowWidth = 720;
  	int rowHeight = 60;

    // items on the menu page
  	char* menu[] = {"MAIN MENU", "Map View", "List View", "Search Flights",
                    "Following", "Settings", "About the credits"};

  	//clear the Screen
  	clearScreen(BLUE);

  	//draw the header first
  	drawHeader();

    // Including the title, draw all the 7 tabs with alternative colours
    // NOTE: patterns are specified in the high level design
    int rowColour = RED;
    int fontColour = WHITE;
  	for (int i=0; i<7; i++) {
  		if (i > 0) {
  			rowColour = (i % 2)? WHITE : GREY;
        fontColour = BLACK;
  		}
      // background
  		drawRectangle(leftMargin, topMargin+i*rowHeight, rowWidth, rowHeight, rowColour);
      // display the text
  		placeTextInCenter(menu[i], topMargin+i*rowHeight+10, 16, fontColour, "16x27");
  	}
}

/*
 * Display the following page, which is basically the same as the list page
 */

void displayFollowPage(Flight flights[], int numFlights)
{
    //draw the backgroud
    clearScreen(BLACK); //Not working now...

    int leftMargin = 45;
    int topMargin  = 50;

    //FLIGHT TRACKER header
    drawHeader();

    //tab background
    int tabWidth = 240;
    int tabHeight= 50;
    int boarder = 2;
    drawRectangleBoarder(leftMargin, topMargin, tabWidth, tabHeight, RED, boarder);
    drawRectangleBoarder(leftMargin+tabWidth+boarder, topMargin, tabWidth, tabHeight, YELLOW, boarder);
    drawRectangleBoarder(leftMargin+2*tabWidth+2*boarder, topMargin, tabWidth, tabHeight, RED, boarder);

    //tab text
    displayText("List", 16, BLACK, 90, 70, "16x27");
    displayText("Following", 16, BLACK, 300, 70, "16x27");
    displayText("Search", 16, BLACK, 560, 70, "16x27");


    //three rows of data
    int rowWidth = 722;
    int rowHeight = 120;
    topMargin += tabHeight;
    drawRectangle(leftMargin, topMargin, rowWidth, rowHeight, WHITE);
    drawRectangle(leftMargin, topMargin+rowHeight, rowWidth, rowHeight, GREY);
    drawRectangle(leftMargin, topMargin+2*rowHeight, rowWidth, rowHeight, WHITE);

    //display the flight info
    int longIndex = 18;
    int latIndex = 39;
    int timeIndex = 66;

    char text[80] = "AC1234       Long:49.43            Lat:84.345        Last Update: 12:42\n";
    numFlights = min(numFlights, 3);
    for (int i=0; i<numFlights; i++) {
        for (int j=0; j<FIDLen; j++) {
            text[j] = flights[i].flightID[j];
        }
        for (int j=0; j<LongLen; j++) {
            text[j+longIndex] = flights[i].longitude[j];
        }
        for (int j=0; j<LatLen; j++) {
            text[j+latIndex] = flights[i].latitude[j];
        }
        for (int j=0; j<TimeLen-7; j++) { //hour:min
            text[j+timeIndex] = flights[i].time[j];
        }
        for (int j=0; j<79; j++) {
            if (text[j] == '\0')
                text[j] = ' ';
        }
        displayText(text, 10, BLACK, leftMargin+10, 130+120*i, "10x14");
    }
}

/*
 * Fucntion to display the map page
 * NOTE: should be completed by Courtney, but she is missing.....
 */

void displayMapPage(Flight* flights, int numFlights)
{
    clearScreen(BLACK);
    drawTriangle(300, 300, 50, WHITE);
}

/*
 * Fucntion to display the credits page
 */
void displayCreditsPage()
{
    clearScreen(GREY); // backgorund
    drawHeader();
    char* credits[] = {"This is a program designed by CPEN391 GROUP 6A",
                       "Developers:", "Alex", "Bill", "Courtney", "Kyler", "RuoChen"};
    int topMargin = 100;
    for (int i=0; i<7; i++){
      placeTextInCenter((const char*)credits[i], topMargin+i*20, 10, BLACK, "10x14");
    }
}
