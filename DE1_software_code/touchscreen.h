#ifndef TOUCHSCREEN_H_
#define TOUCHSCRENN_H_

#include "flightTracker.h"

#define HW_REGS_BASE ( 0xff200000 )
#define HW_REGS_SPAN ( 0x00200000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )
#define Touchscreen_ReceiverFifo (*(volatile unsigned char *) ((virtual_base + ((0x10230 ) &  (HW_REGS_MASK )))))
#define Touchscreen_TransmitterFifo (*(volatile unsigned char *) ((virtual_base + ((0x10230 ) &  (HW_REGS_MASK )))))
#define Touchscreen_InterruptEnableReg (*(volatile unsigned char *) ((virtual_base + ((0x10232 ) &  (HW_REGS_MASK )))))
#define Touchscreen_InterruptIdentificationReg (*(volatile unsigned char *) ((virtual_base + ((0x10234 ) &  (HW_REGS_MASK )))))
#define Touchscreen_FifoControlReg (*(volatile unsigned char *) ((virtual_base + ((0x10234 ) &  (HW_REGS_MASK )))))
#define Touchscreen_LineControlReg (*(volatile unsigned char *) ((virtual_base + ((0x10236 ) &  (HW_REGS_MASK )))))
#define Touchscreen_ModemControlReg (*(volatile unsigned char *) ((virtual_base + ((0x10238 ) &  (HW_REGS_MASK )))))
#define Touchscreen_LineStatusReg (*(volatile unsigned char *) ((virtual_base + ((0x1023A ) &  (HW_REGS_MASK )))))
#define Touchscreen_ModemStatusReg (*(volatile unsigned char *) ((virtual_base + ((0x1023C ) &  (HW_REGS_MASK )))))
#define Touchscreen_ScratchReg (*(volatile unsigned char *) ((virtual_base + ((0x1023E ) &  (HW_REGS_MASK )))))
#define Touchscreen_DivisorLatchLSB (*(volatile unsigned char *) ((virtual_base + ((0x10230 ) &  (HW_REGS_MASK )))))
#define Touchscreen_DivisorLatchMSB (*(volatile unsigned char *) ((virtual_base + ((0x10232 ) &  (HW_REGS_MASK )))))

/* a data type to hold a point/coord */
typedef struct { int x, y; } Point ;

//all function headers

//initialize the RS232
void Init_Touchscreen(void);

//write data into the buffer
int putcharTouchscreen(int c);

//read date from the buffer
int getcharTouchscreen(void);

// the following function polls the UART to determine if any character
// has been received. It doesn't wait for one, or read it, it simply tests
// to see if one is available to read from the FIFO
int TouchscreenTestForReceivedData(void);

// Remove/flush the UART receiver buffer by removing any unread characters
void TouchscreenFlush(void);

// Initialise touch screen controller
void Init_Touch(void);

//test if screen is simply touched (regardless of event type)
int ScreenTouched(void);

//wait for screen to be touched
void WaitForTouch();

//This function waits for a touch screen press event and returns X,Y coord
Point GetPress(void);

//This function waits for a touch screen release event and returns X,Y coord
Point GetRelease(void);

//This function waits for a touch screen including both press and release events and returns X,Y coord
Point GetTouched(void);

#endif
