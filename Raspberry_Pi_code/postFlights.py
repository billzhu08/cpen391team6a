from time import ctime
import time
import urllib.request
import requests
import json
import re

#Fetch data from decoder json file, and send flight data to backend server
def postFlights():
    abc = requests.delete('https://warm-brook-40519.herokuapp.com/request')
    with open('/var/run/dump1090-mutability/aircraft.json') as json_file:
        data = json.load(json_file)
    flights = data['aircraft']

    dict = {}
    for f in flights:
        if "flight" in f and "lat" in f and "lon" in f and "altitude" in f and "speed" in f:
            dict[f['hex']] = f
    json_file.close()
    
    url = 'https://warm-brook-40519.herokuapp.com/flights'     
    timehehe = ctime()
    timehehe = timehehe[11:16]
    print(timehehe)
    
    for x in dict:
        myFlight = {
            'flight': dict[x]["flight"],
            'latitude': dict[x]["lat"],
            'longtitude': dict[x]["lon"],
            'altitude': dict[x]["altitude"],
            'speed': dict[x]["speed"],
            'date2': " ",
            'time2': timehehe
            }
        print(timehehe)
        print(myFlight)
        requests.post(url, data = myFlight)
        print('flight post finished!')
        
#wait for the next server request, if we get that request from server, then call postFlights() to send data, otherwise wait
#for other request from server
def wait():
    fp = urllib.request.urlopen("https://warm-brook-40519.herokuapp.com/request")
    mybytes = fp.read()
    mystr = mybytes.decode("utf8")
    fp.close()
    pos = mystr.find('id')
    while pos < 0:
        fp = urllib.request.urlopen("https://warm-brook-40519.herokuapp.com/request")
        mybytes = fp.read()
        mystr = mybytes.decode("utf8")
        fp.close()
        pos = mystr.find('id')
        time.sleep(1)
        print("Printed after 1 seconds.")
    requests.delete('https://warm-brook-40519.herokuapp.com/flights')
    postFlights()
    requests.delete('https://warm-brook-40519.herokuapp.com/request')
    wait()
wait()



