//declare all the varibles to be used
var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    mongoose = require("mongoose"),
    methodOverride = require("method-override"),
    Flight = require("./models/flights"),
    Request = require("./models/request")


//connect to MongoDB Atlas
mongoose.connect('mongodb+srv://alexDB:lyz19950912@cluster0-k44mz.mongodb.net/flights?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useCreateIndex: true
}).then(() => {
    console.log("Connected to cloud DB!");
}).catch(err => {
    console.log('ERR:', err.message);
});


//Node JS set up c
app.use(bodyParser.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.use(methodOverride("_method"));

//landing page route
app.get("/", function(req, res) {
    res.render("landing");
});


//route that show all flights from MongoDB
app.get("/flights", async function(req, res) {
    try {
        var allFlights = await Flight.find({});
        var ans = JSON.stringify(allFlights);
        res.send(ans);
    } catch (err) {
        console.log(err);
        res.send(err);
    }
});

//input flights route. For test use
app.get("/flights/input", function(req, res) {
    res.render("input");
});


//post route that post one flight onto MongoDB
app.post("/flights", async function(req, res) {

    var flight = req.body.flight;
    var date2 = req.body.date2;
    var time2 = req.body.time2;
    var latitude = req.body.latitude;
    var longtitude = req.body.longtitude;
    var altitude = req.body.altitude;
    var speed = req.body.speed;

    var newFlight = {
        flight: flight,
        date2: date2,
        time2: time2,
        latitude: latitude,
        longtitude: longtitude,
        altitude: altitude,
        speed: speed
    };

    try {
        await Flight.create(newFlight);
        res.redirect("/flights");
    } catch (err) {
        console.log(err);
        res.redirect("/flights/input");
    }

});

//send request route. For test use
app.get("/request/send", function(req, res) {
    res.render("requestSend");
});

//request show page. For test use
app.get("/request", async function(req, res) {
    try {
        var request = await Request.find({});
        var ans = JSON.stringify(request);
        res.send(ans);
    } catch (err) {
        console.log(err);
        res.send(err);
    }
});


//post route that send a request to MongoDB
app.post("/request", async function(req, res) {

    var request = req.body.request;
    var newRequest = { request: request };


    try {
        await Request.create(newRequest);
        res.redirect("/flights/input");
    } catch (err) {
        console.log(err);
        res.redirect("/flights/input");
    }

});

//delete route to delete request in MongoDB
app.delete("/request", function(req, res) {
    Request.remove({}, function(err) {

        if (err) {
            console.log(err);

        } else {
            console.log("Request removed!");
            res.redirect("/flights/input");
        }
    });
});

//delete route to delte all flights in MongoDB
app.delete("/flights", function(req, res) {
    Flight.remove({}, function(err) {

        if (err) {
            console.log(err);

        } else {
            console.log("Flights removed!");
            res.redirect("/flights");
        }
    });
});


//listen to alll requests 
app.listen(process.env.PORT, process.env.IP, function() {
    console.log("CPEN391 has started!!")
});