var mongoose = require("mongoose");

var flightSchema = new mongoose.Schema({
	// msg: String,
	// num1: String,
	// num2: String,
	// num3: String,
	flight: String,
	// Num4: String,
	// Date1: String,
	// Time1: String,
	date2: String,
	time2: String,
	// Num5: String,
	// Num6: String,
	// Num7: String,
	// Num8: String,
	latitude: String,
	longtitude: String,
	altitude: String,
	speed: String
	// Num9: String,
	// Num10: String,
	// Num11: String,
	// Num12: String,
	// Num13: String,
	// Num14: String,
});

module.exports = mongoose.model("Flight", flightSchema);