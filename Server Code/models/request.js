var mongoose = require("mongoose");

var requestSchema = new mongoose.Schema({
	request: String
});

module.exports = mongoose.model("Request", requestSchema);